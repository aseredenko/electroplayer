# README #

### ElectroPlayer ###
![logo.png](https://bitbucket.org/repo/GggBXL6/images/2768185645-logo.png)

ElectroPlayer is cross-platform application for playing you movies and videos. You can watch local files and videos from the Internet.

### Supported formats: ###

* mp4
* ogg
* webm