(function() {

  var app = angular.module('app', []);

  app.controller('appCtrl', ['$scope', '$http', function($scope, $http) {
    const { dialog } = require('electron').remote;
    const notifier = require('node-notifier');

    var URL = window.URL || window.webkitURL;
    var video = document.querySelector('video');
    window.onresize = setVideoHeight;



    function setVideoHeight() {
      document.getElementById('videoPlayer').style.height = window.innerHeight + 'px';
    }

    function checkSupporting(format) {
      var isSupport = video.canPlayType(format) !== '';
      return isSupport;
    }

    function showDialog(text, type) {
      dialog.showMessageBox({
        type: type,
        title: 'Error!',
        message: text,
        buttons: ["Close"]
      });
    }

    function showNotification(title, options) {
      Notification.requestPermission(function(permission) {
          var notification = new Notification(title, options);
      });
    }

    $scope.goToUploadWindow = function() {
      $scope.uploadSuccess = false;

      var videoNode = document.querySelector('video');
      videoNode.src = '';

      document.getElementById('videoFileInput').value = '';
    };

    $scope.playVideoFromUrl = function(url) {
      // Example: https://www.w3schools.com/html/mov_bbb.mp4

      setVideoHeight();

      var type = url.split('.').pop(); // Get format from URL
      var isSupport = checkSupporting('video/' + type);

      if (isSupport) {
        showNotification("Upload Success!", {
          body: "Enjoy your watching",
          icon: "http://www.freeiconspng.com/uploads/success-icon-10.png"
        });

        $scope.uploadSuccess = true;
        video.src = url;
      } else {
        showDialog("You have uploaded unsupported format. Please upload mp4/webm/ogg format.", "error");
      }
    }


    $scope.playSelectedFile = function(element) {
      var file = element.files[0];
      var type = file.type;
      var videoNode = document.querySelector('video');

      var isFileSupported = checkSupporting(type);

      if (!isFileSupported) {
        showDialog("You have uploaded unsupported format. Please upload mp4/webm/ogg format.", "error");

        $scope.$apply(function() {
          $scope.uploadSuccess = false;
        });

      } else {

        showNotification("Upload Success!", {
          body: "Enjoy your watching",
          icon: "http://www.freeiconspng.com/uploads/success-icon-10.png"
        });

        setVideoHeight();
        $scope.$apply(function() {
          $scope.uploadSuccess = true;
        });

        var fileURL = URL.createObjectURL(file);
        videoNode.src = fileURL;
      }
    }

  }]);
})();
